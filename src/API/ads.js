import axios from "axios";
import { getCookie } from "@/utils";

const adsInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1/ads",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("jwt")
    }
})

export const fetchAds = () => {
    return adsInstance.get()
}
