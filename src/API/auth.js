import axios from "axios";

const authInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1",
    headers: {
        "Content-Type": "application/json"
    }
})

export const authUser = (email, password) => {
    return authInstance.post("/auth", { email, password })
}


