import axios from "axios";
import { getCookie } from "@/utils";

const citiesInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1/cities",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("jwt")
    }
})

export const cities = {
    fetchCities() {
        return citiesInstance.get()
    }
}