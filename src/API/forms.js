import axios from "axios";
import { getCookie } from "@/utils";

const formsInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1/forms",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("jwt")
    }
})

export const formsAPI = {
    fetchForms: ({ keyword = "" }) => formsInstance.get(`?include=fields,status&filter[title]=${keyword}`),
    fetchForm: ({ formId }) => formsInstance.get(`/${formId}?include=fields,status`),
    updateForm: ({ formId, formData }) => formsInstance.post(`/${formId}`, JSON.stringify(formData)),
    changeField: ({ formId, field }) => formsInstance.post(`/${formId}/fields/${field.id}`, JSON.stringify(field)),
    createField: ({ formId, field }) => formsInstance.post(`/${formId}/fields`, JSON.stringify(field)),
    deleteField: ({ formId, fieldId }) => formsInstance.delete(`/${formId}/fields/${fieldId}`)
}