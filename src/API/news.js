import axios from "axios";
import { getCookie } from "@/utils";

const newsInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1/articles",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("jwt")
    }
})

export const newsAPI = {
    fetchPosts: ({ count = "", dateFrom = "", dateTo = "", regionId = "", categoryId = "", keyword = "", page = "" }) =>
        newsInstance.get(`?per_page=${count}&page=${page}&filter[published_after]=${dateFrom}&filter[published_before]=${dateTo}&filter[region_id]=${regionId}&filter[category_id]=${categoryId}&filter[title]=${keyword}`),
    fetchPostById: postId => newsInstance.get(`/${postId}?include=categories`),
    fetchCategories: () => newsInstance.get("/filter-categories"),
    saveNews: ({ contentHTML, contentText, title, categories, date, type, region, previewImage }) => {
        const newsFormData = new FormData()
        newsFormData.append("title", title)
        newsFormData.append("preview_image", previewImage)
        newsFormData.append("detail_text", contentHTML)

        const previewTextArr = contentText.slice(0, 51).split(" ")
        newsFormData.append("preview_text", previewTextArr.slice(0, previewTextArr.length - 1).join(" ") + "...")

        region && newsFormData.append("regions[0]", region)

        for (let i = 0; i < categories.length; i++) {
            newsFormData.append(`categories[${i}]`, categories[i])
        }
        newsFormData.append("published_at", date)

        return newsInstance.post('', newsFormData)
    }
}