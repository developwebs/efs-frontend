import axios from "axios";
import { getCookie } from "@/utils";

const organizationsInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1/organizations",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("jwt")

    }
})

export const fetchOrganizations = () => organizationsInstance.get()
export const fetchOrganizationsByRegionId = regionId => axios.get(`http://api.efs.demostorage.site/api/v1/regions/${regionId}?include=organizations`, {
    headers: {
        "Authorization": "Bearer" + getCookie("jwt")
    }
})
export const fetchOrganizationById = organizationId => organizationsInstance.get(`/${organizationId}?include=employees,educationPrograms`)
export const fetchOrganizationEmployees = organizationId => organizationsInstance.get(`/${organizationId}/employees`)

