import axios from "axios";
import { getCookie } from "@/utils";

const partnersInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1/partners",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("jwt")
    }
})

export const fetchPartners = () => partnersInstance.get()