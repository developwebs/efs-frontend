import axios from "axios";
import { getCookie } from "@/utils";

const regionsInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1/regions",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("jwt")
    }
})

export const fetchRegions = ({keyword = "", startsWith = ""} = {}) => {
    return regionsInstance.get(`?filter[title]=${keyword}&filter[starts_with]=${startsWith}`)
}
export const fetchRegionStat = regionId => regionsInstance.get(`/${regionId}/stats`)
export const fetchRegion = regionId => regionsInstance.get(`/${regionId}`)