import axios from "axios"
import { getCookie } from "@/utils";

const technicalSupportInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1/support",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("jwt")
    }
})

export const sendTechnicalSupportMessage = messageData => technicalSupportInstance.post("/submit", messageData)