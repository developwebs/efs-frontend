import axios from "axios"
import { getCookie } from "@/utils"

const usersInstance = axios.create({
    baseURL: "http://api.efs.demostorage.site/api/v1/users",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("jwt")
    }
})

export const fetchUser = () => usersInstance.get(`/current`)

export const updateUser = ({ id, first_name = "", last_name = "", second_name = "", image = "" }) => {
    const userData = new FormData()
    userData.append("first_name", first_name)
    userData.append("last_name", last_name)
    userData.append("second_name", second_name)
    userData.append("image", image)

    return axios.post(`http://api.efs.demostorage.site/api/v1/users/${id}`, userData, {
        headers: {
            "Authorization": "Bearer " + '194|fl5OnqvyKV3ZF8WwjCJafPvvvTVd8EJEaGpF7HZE'
        }
    })
}

export const updatePassword = ({ newPassword, oldPassword, passwordConfirm }) => usersInstance.post(`/current/password`, JSON.stringify({ password: oldPassword, new_password: newPassword, confirm_password: passwordConfirm }))
export const fetchUsers = ({ keyword, regionId, cityId, dateFrom, dateTo }) => usersInstance.get(`?include=organization,region,city,position,roles&filter[city_id]=${cityId}&filter[region_id]=${regionId}&filter[name]=${keyword}&filter[last_login_after]=${dateFrom}&filter[last_login_before]=${dateTo}`)