import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store/store'
import { plugin as VueTippy } from 'vue-tippy'
import 'tippy.js/dist/tippy.css'
import components from "@/components/UI"
import { QuillEditor } from '@vueup/vue-quill'
import '@vueup/vue-quill/dist/vue-quill.snow.css';


const app = createApp(App)
components.forEach(component => {
    app.component(component.name, component)
})

app
    .use(router)
    .use(store)
    .use(
        VueTippy,
        // optional
        {
            directive: 'tippy', // => v-tippy
            component: 'tippy', // => <tippy/>
            componentSingleton: 'tippy-singleton', // => <tippy-singleton/>,
            defaultProps: {
                placement: 'auto-end',
                allowHTML: true,
                trigger: "click"
            }, // => Global default options * see all props
        }
    )
    .component("QuilEditor", QuillEditor)
    .mount("#app")
