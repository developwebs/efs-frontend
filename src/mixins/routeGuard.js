export const routeGuard = {
    methods: {
        routeGuard(to, vm) {
            const authRoutes = ["/cabinet"];
            const unAuthRoutes = ["/auth"];
            const isAuth = vm.$store.state.auth.auth;
            console.log(isAuth)

            if (
                authRoutes.some((authRoute) => to.path.startsWith(authRoute)) &&
                !isAuth
            ) {
                vm.$router.push("/auth")

                console.log("auth")
                return;
            }
            if (
                unAuthRoutes.some((authRoute) => to.path.startsWith(authRoute)) &&
                isAuth
            ) {
                vm.$router.push("/cabinet")
                console.log("cabinet")
                return;
            }

            vm.$router.push(to);
        }
    }
}