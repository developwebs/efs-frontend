// views
import Public from "@/views/Public.vue"
import Cabinet from "@/views/Cabinet.vue"
// components
import Main from "@/pages/Public/Main.vue"
import Auth from "@/pages/Public/Auth.vue"
import News from "@/pages/Public/News.vue"
import NewsDetail from "@/pages/Public/NewsDetail.vue"
import NotFounded from "@/pages/Public/NotFounded.vue"
import RegionsDetail from "@/pages/Public/Organizations/RegionDetail.vue"
import Regions from "@/pages/Public/Organizations/Regions.vue"
import Organization from "@/pages/Public/Organizations/Organization.vue"

import CabinetProfile from "@/pages/Cabinet/CabinetProfile/CabinetProfile.vue"
import CabinetMap from "@/pages/Cabinet/CabinetMap/CabinetMap.vue"
import CabinetRegions from "@/pages/Cabinet/CabinetRegions/CabinetRegions.vue"
import CabinetRegionDetail from "@/pages/Cabinet/CabinetRegionDetail/CabinetRegionDetail.vue"
import CabinetOrganizationDetail from "@/pages/Cabinet/CabinetOrganizationDetail/CabinetOrganizationDetail.vue"
import CabinetReports from "@/views/Cabinet/CabinetReports.vue"
import CabinetHistoryRequests from "@/pages/Cabinet/CabinetReports/CabinetHistoryRequests.vue"
import CabinetTemplates from "@/pages/Cabinet/CabinetReports/CabinetTemplates.vue"
import CabinetReportsDetail from "@/pages/Cabinet/CabinetReports/CabinetReportsDetail.vue"
import CabinetListeners from "@/pages/Cabinet/CabinetListeners/CabinetListeners.vue"
import CabinetNotifications from "@/pages/Cabinet/CabinetNotifications/CabinetNotifications.vue"
import CabinetAddNewsPage from "@/pages/Cabinet/CabinetAddNewsPage/CabinetAddNewsPage.vue"
import CabinetForms from "@/pages/Cabinet/CabinetForms/CabinetForms.vue"
import CabinetFormDetail from "@/pages/Cabinet/CabinetFormDetail/CabinetFormDetail.vue"
import CabinetUsers from "@/pages/Cabinet/CabinetUsers/CabinetUsers.vue"

export default [
    {
        path: "/",
        component: Public,
        children: [
            {
                path: "",
                component: Main
            },
            {
                path: "auth",
                component: Auth
            },
            {
                path: "news",
                component: News
            },
            {
                path: "news/:id",
                component: NewsDetail
            },
            {
                path: "organizations",
                component: Regions
            },
            {
                path: "organizations/:regionId",
                component: RegionsDetail
            },
            {
                path: "organizations/:regionId/:organizationId",
                component: Organization
            }
        ]
    },
    {
        path: "/cabinet",
        component: Cabinet,
        name: "cabinet",
        children: [
            {
                path: "",
                component: CabinetProfile,
                name: "cabinet profile"
            },
            {
                path: "map",
                component: CabinetMap,
                name: "cabinet map"
            },
            {
                path: "organizations",
                component: CabinetRegions,
                name: "cabinet regions"
            },
            {
                path: "organizations/:regionId",
                component: CabinetRegionDetail
            },
            {
                path: "organizations/:regionId/:organizationId",
                component: CabinetOrganizationDetail
            },
            {
                path: "forms",
                component: CabinetForms
            },
            {
                path: "forms/form-:formId",
                component: CabinetFormDetail
            },
            {
                path: "listeners",
                component: CabinetListeners
            },
            {
                path: "notifications",
                component: CabinetNotifications
            },
            {
                path: "add-news",
                component: CabinetAddNewsPage
            },
            {
                path: "users",
                component: CabinetUsers
            },
            {
                path: "reports",
                component: CabinetReports,
                children: [
                    {
                        path: "",
                        redirect: "/cabinet/reports/reports-detail"
                    },
                    {
                        path: "reports-detail",
                        component: CabinetReportsDetail
                    },
                    {
                        path: "templates",
                        component: CabinetTemplates
                    },
                    {
                        path: "history-requests",
                        component: CabinetHistoryRequests
                    }
                ]
            }
        ]
    },
    {
        path: "/:catchAll(.*)",
        component: NotFounded,
    }
]