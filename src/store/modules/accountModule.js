import { fetchUser } from "@/API/users";

export const accountModule = {
    state() {
        return {
            userData: null,
        }
    },
    mutations: {
        setUserData(state, userData) {
            state.userData = userData
        }
    },
    actions: {
        async getUser({ commit }) {
            const { data: userData } = await fetchUser()

            commit("setUserData", userData)
            
            commit("profile/setFormData", {...userData}, { root: true })
            commit("auth/setAuth", true, { root: true })
        }
    },
    modules: {

    },
    namespaced: true
}