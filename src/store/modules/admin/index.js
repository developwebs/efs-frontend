import { addNewsModule } from "./addNewsModule"

export const adminModule = {
    modules: {
        addNews: addNewsModule
    },
    namespaced: true
}