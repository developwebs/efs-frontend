import { formsAPI } from "@/API/forms"

export const formDetailModule = {
    state() {
        return {
            form: {},
            fields: [],
            formId: null
        }
    },
    mutations: {
        setForm(state, form) {
            state.form = form
        },
        setFields(state, fields) {
            state.fields = fields
        },
        setFieldTitle(state, fieldData) {
            const newField = { ...state.fields.find(field => field.id == fieldData.id) }
            const fieldIndex = state.fields.findIndex(field => field.id == fieldData.id)
            newField.title = fieldData.title
            state.fields = [...state.fields.slice(0, fieldIndex), newField, ...state.fields.slice(fieldIndex + 1)]
        },
        setFieldDescription(state, fieldData) {
            const newField = { ...state.fields.find(field => field.id == fieldData.id) }
            const fieldIndex = state.fields.findIndex(field => field.id == fieldData.id)
            newField.description = fieldData.description
            state.fields = [...state.fields.slice(0, fieldIndex), newField, ...state.fields.slice(fieldIndex + 1)]
        },
        setFieldType(state, fieldData) {
            const newField = { ...state.fields.find(field => field.id == fieldData.id) }
            const fieldIndex = state.fields.findIndex(field => field.id == fieldData.id)
            newField.type = fieldData.type
            state.fields = [...state.fields.slice(0, fieldIndex), newField, ...state.fields.slice(fieldIndex + 1)]
        },
        setFieldRequired(state, fieldData) {
            const newField = { ...state.fields.find(field => field.id == fieldData.id) }
            const fieldIndex = state.fields.findIndex(field => field.id == fieldData.id)
            newField.required = +fieldData.required
            state.fields = [...state.fields.slice(0, fieldIndex), newField, ...state.fields.slice(fieldIndex + 1)]
            console.log(newField)
        },
        setFormId(state, formId) {
            state.formId = formId
        }
    },
    actions: {
        async getForm({ commit, state }) {
            const { data: { data: form } } = await formsAPI.fetchForm({ formId: state.formId })
            commit("setForm", form)
            commit("setFields", form.fields)
        },
        async saveFields({ state, dispatch }) {
            await formsAPI.updateForm({
                formId: state.formId, formData: {
                    title: state.form.title,
                    fields: state.fields.map(field => ({
                        title: field.title,
                        description: field.description,
                        type: field.type,
                        required: field.required,
                        sort: field.sort,
                        enabled: field.enabled
                    }))
                }
            })
            dispatch("getForm")

        },
        async createField({ state, dispatch }, field) {
            await formsAPI.createField({ formId: state.formId, field })
            dispatch("getForm")
        },
        async deleteField({ state, dispatch }, fieldId) {
            await formsAPI.deleteField({ formId: state.formId, fieldId })
            console.log(11)
            dispatch("getForm")
        },
        async toggleEnabledField({ state, dispatch }, field) {
            field = { ...field }
            field.enabled = !field.enabled
            await formsAPI.changeField({ formId: state.formId, field })
            dispatch("getForm")
        }
    },
    namespaced: true
}