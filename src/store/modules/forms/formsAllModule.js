import { formsAPI } from "@/API/forms"

export const formsAllModule = {
    state() {
        return {
            forms: [],
            filters: {
                keyword: ""
            }
        }
    },
    mutations: {
        setForms(state, forms) {
            state.forms = forms
        },
        setKeyword(state, keyword) {
            state.filters = {
                ...state.filters,
                keyword
            }
        }
    },
    actions: {
        async getForms({ commit, state }) {
            const { data: { data: forms } } = await formsAPI.fetchForms({ keyword: state.filters.keyword })
            commit("setForms", forms)
        }
    },
    namespaced: true
}