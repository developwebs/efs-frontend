import { formsAllModule } from "./formsAllModule"
import { formDetailModule } from "./formDetailModule"

export const formsModule = {
    modules: {
        formsAll: formsAllModule,
        formDetail: formDetailModule,
    },
    namespaced: true
}