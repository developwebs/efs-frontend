import { fetchRegions } from "@/API/regions";
import { cities } from "../../API/cities";

export const locationModule = {
    state() {
        return {
            regions: {
                regions: [],
                loaded: false
            },
            cities: {
                cities: [],
                loaded: false
            }
        }
    },
    mutations: {
        setRegions(state, regions) {
            state.regions = {
                ...state.regions,
                regions
            }
        },
        setRegionsLoaded(state, loaded) {
            state.regions = {
                ...state.regions,
                loaded
            }
        },
        setCities(state, cities) {
            state.cities = {
                ...state.cities,
                cities
            }
        },
        setCitiesLoaded(state, loaded) {
            state.cities = {
                ...state.cities,
                loaded
            }
        }
    },
    actions: {
        async getRegions({ commit }) {
            const { data } = await fetchRegions()
            commit("setRegions", data.data.data)
            commit("setCitiesLoaded", true)
        },
        async getCities({ commit }) {
            const { data } = await cities.fetchCities()
            commit("setCities", data.data.data)
            commit("setCitiesLoaded", true)
        }
    },
    namespaced: true
}