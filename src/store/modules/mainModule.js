import { fetchAds } from "@/API/ads";
import { fetchPartners } from "@/API/partners";
import { newsAPI } from "@/API/news";

export const mainModule = {
    state() {
        return {
            posts: [],
            partners: [],
            ads: []
        }
    },
    mutations: {
        setPosts(state, posts) {
            state.posts = posts
        },
        setPartners(state, partners) {
            state.partners = partners
        },
        setAds(state, ads) {
            state.ads = ads.map(ad => ({
                ...ad,
                image: "http://api.efs.demostorage.site/storage" + ad.image
            }))
        }
    },
    getters: {
        getPosts(state) {
            return state.posts
        }
    },
    actions: {
        getPosts: async function ({ commit }) {
            const { data } = await newsAPI.fetchPosts({ count: 3 })
            commit("setPosts", data.data.data)
        },
        getPartners: async function ({ commit }) {
            const { data } = await fetchPartners()
            commit("setPartners", data.data.data)
        },
        getAds: async function ({ commit }) {
            const { data } = await fetchAds()
            commit("setAds", data.data)
        }
    },
    modules: {

    },
    namespaced: true
}