import { newsAllModule } from "./newsAllModule";
import { newsDetailModule } from "./newsDetailModule";

export const newsModule = {
    modules: {
        newsAll: newsAllModule,
        newsDetail: newsDetailModule,
    },
    namespaced: true
}