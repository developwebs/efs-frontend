import { newsAPI } from "@/API/news";

export const newsDetailModule = {
    state() {
        return {
            mainPost: null,
            pagination: null,
            otherPosts: [],
        }
    },
    mutations: {
        setMainPost(state, mainPost) {
            state.mainPost = mainPost
        },
        addOtherPosts(state, posts) {
            state.otherPosts = posts
        }
    },
    actions: {
        getPostById: async function ({ commit }, postId) {
            const { data: { data: mainPost } } = await newsAPI.fetchPostById(postId.split("-")[0])

            commit("setMainPost", mainPost)
        },
        getOtherPosts: async function ({ commit }, count) {
            const { data: { data: { data: otherPosts } } } = await newsAPI.fetchPosts(count)

            commit("addOtherPosts", otherPosts)
        },
    },
    namespaced: true
}