import { regionsModule } from "./regionsModule";
import { regionInfoModule } from "./regionInfoModule";
import { organizationInfoModule } from "./organizationInfoModule";

export const organizationsModule = {
    state() {

    },
    modules: {
        organizationsRegions: regionsModule,
        organizationsRegionInfo: regionInfoModule,
        organizationInfo: organizationInfoModule
    },
    namespaced: true
}