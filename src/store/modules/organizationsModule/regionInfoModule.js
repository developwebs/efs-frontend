import { fetchOrganizationEmployees, fetchOrganizationsByRegionId } from "@/API/organizations"
import { fetchRegion, fetchRegionStat } from "@/API/regions"

export const regionInfoModule = {
    state() {
        return {
            organizations: [],
            regionStat: [],
            regionId: null,
            region: null
        }
    },
    mutations: {
        setOrganizations(state, organizations) {
            state.organizations = organizations
        },
        setRegionStat(state, regionStat) {
            state.regionStat = regionStat
        },
        setRegionId(state, regionId) {
            state.regionId = regionId
        },
        setRegion(state, region) {
            state.region = region
        }
    },
    actions: {
        async getOrganizations({ commit, state }, withEmployees) {
            let { data: { data: { organizations } } } = await fetchOrganizationsByRegionId(state.regionId)
            if (withEmployees) {
                await Promise.all(organizations.map(async organization => {
                    const { data: { data: { data: allEmployees } } } = await fetchOrganizationEmployees(organization.id)
                    organization.employees = allEmployees.filter(employee => employee.sort !== 1)
                    organization.mainEmployee = allEmployees.find(employee => employee.sort === 1)
                }))
            }

            commit("setOrganizations", organizations)
        },
        async getRegionStat({ commit, state }) {
            const sortData = [
                "Учителей",
                "Слушатели Академии ",
                "Слушатели ЦНППМ",
                "Тьюторов Нацпроекта",
                "Организаций ДПО в Федеральном реестре",
                "Программы ДПО, поданные в Федеральный реестр",
                "Программ ДПО в Федеральном реестре",
                "Федеральных экспертов"
            ]

            const { data: { data: regionStat } } = await fetchRegionStat(state.regionId)
            console.log(regionStat)
            const newRegionStat = sortData.map(title => (
                {
                    title,
                    value: regionStat.find(statPart => statPart.title == title)?.value,
                    code: regionStat.find(statPart => statPart.title == title)?.code
                }))
            console.log(newRegionStat)
            commit("setRegionStat", newRegionStat)
        },
        async getRegion({ commit, state }) {
            const { data: { data: region } } = await fetchRegion(state.regionId)
            commit("setRegion", region)
        }

    },
    namespaced: true
}