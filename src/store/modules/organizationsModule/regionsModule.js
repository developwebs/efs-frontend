import { fetchRegions } from "@/API/regions";

export const regionsModule = {
    state() {
        return {
            regions: [],
            charStatuses: {
                "А": {
                    active: false,
                    disabled: true,
                },
                "Б": {
                    active: false,
                    disabled: false,
                },
                "В": {
                    active: false,
                    disabled: true,
                },
                "Г": {
                    active: false,
                    disabled: true,
                },
                "Д": {
                    active: false,
                    disabled: true,
                },
                "Е": {
                    active: false,
                    disabled: true,
                },
                "Ё": {
                    active: false,
                    disabled: true,
                },
                "Ж": {
                    active: false,
                    disabled: true,
                },
                "З": {
                    active: false,
                    disabled: true,
                },
                "И": {
                    active: false,
                    disabled: true,
                },
                "Й": {
                    active: false,
                    disabled: true,
                },
                "К": {
                    active: false,
                    disabled: true,
                },
                "Л": {
                    active: false,
                    disabled: true,
                },
                "М": {
                    active: false,
                    disabled: true,
                },
                "Н": {
                    active: false,
                    disabled: true,
                },
                "О": {
                    active: false,
                    disabled: true,
                },
                "П": {
                    active: false,
                    disabled: true,
                },
                "Р": {
                    active: false,
                    disabled: false,
                },
                "С": {
                    active: true,
                    disabled: false,
                },
                "Т": {
                    active: false,
                    disabled: true,
                },
                "У": {
                    active: false,
                    disabled: true,
                },
                "Ф": {
                    active: false,
                    disabled: true,
                },
                "Х": {
                    active: false,
                    disabled: false,
                },
                "Ц": {
                    active: false,
                    disabled: true,
                },
                "Ч": {
                    active: false,
                    disabled: true,
                },
                "Ш": {
                    active: false,
                    disabled: true,
                },
                "Щ": {
                    active: false,
                    disabled: true,
                },
                "Ъ": {
                    active: false,
                    disabled: true,
                },
                "Ы": {
                    active: false,
                    disabled: true,
                },
                "Ь": {
                    active: false,
                    disabled: true,
                },
                "Э": {
                    active: false,
                    disabled: true,
                },
                "Ю": {
                    active: false,
                    disabled: true,
                },
                "Я": {
                    active: false,
                    disabled: true,
                }
            },
            currentChar: "С"
        }
    },
    mutations: {
        setRegions(state, regions) {
            state.regions = regions
        },
        setCurrentChar(state, char) {
            const charStatuses = JSON.parse(JSON.stringify(state.charStatuses))
            Object.keys(charStatuses).forEach(char => charStatuses[char] = {
                ...charStatuses[char],
                active: false
            })

            state.charStatuses = {
                ...charStatuses,
                [char]: {
                    ...state.charStatuses[char],
                    active: true
                }
            }
            state.currentChar = char
        }
    },
    actions: {
        getRegions: async function ({ commit, state }, keyword) {
            const availableRegions = ["Свердловская область", "Ханты-Мансийский автономный округ - Югра", "Самарская область", "Белгородская область", "Республика Татарстан"]
            const { data: {data: {data: regions}} } = await fetchRegions(keyword ? { keyword } : { startsWith: state.currentChar })
            commit("setRegions", regions.filter(region => availableRegions.includes(region.title)))
        }
    },
    namespaced: true
}