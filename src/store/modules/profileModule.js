import { updatePassword, updateUser } from "../../API/users";

export const profileModule = {
    state() {
        return {
            formData: null,
            formPassword: {
                oldPassword: "",
                newPassword: "",
                passwordConfirm: ""
            }
        }
    },
    mutations: {
        setFormData(state, formData) {
            state.formData = formData
        },
        setPasswordConfirm(state, passwordConfirm) {
            state.formPassword = {
                ...state.formPassword,
                passwordConfirm
            }
        },
        setNewPassword(state, newPassword) {
            state.formPassword = {
                ...state.formPassword,
                newPassword
            }
        },
        setOldPassword(state, oldPassword) {
            state.formPassword = {
                ...state.formPassword,
                oldPassword
            }
        },
        setFirstName(state, firstName) {
            state.formData = {
                ...state.formData,
                first_name: firstName
            }
        },
        setSecondName(state, secondName) {
            state.formData = {
                ...state.formData,
                second_name: secondName
            }
        },
        setLastName(state, lastName) {
            state.formData = {
                ...state.formData,
                last_name: lastName
            }
        }
    },
    actions: {
        setUserData({ commit }, userData) {
            commit("account/setUserData", userData, { root: true })
        },
        async saveUserData({ commit, state, dispatch }) {
            const { data: { data: userData } } = await updateUser(state.formData)
            commit("setFormData", userData)
            dispatch("setUserData", userData)
        },
        async saveUserImage({ commit, dispatch, rootState }, image) {
            const { ...userData } = rootState.account.userData
            userData.image = image

            const { data: { data: updatedUserData } } = await updateUser(userData)
            commit("setFormData", updatedUserData)
            dispatch("setUserData", updatedUserData)
        },
        async changePassword({ state }) {
            console.log(1)
            if (!state.formData) {
                return
            }

            console.log(2)

            const { newPassword, passwordConfirm, oldPassword } = state.formPassword

            if (newPassword !== passwordConfirm) {
                alert("Пароли не равны")
                return
            }

            if (newPassword.length < 6) {
                alert("Длина пароля должна быть больше шести символов")
            }

            await updatePassword({id: state.formData.id, newPassword, oldPassword, passwordConfirm})
        }
    },
    namespaced: true
}