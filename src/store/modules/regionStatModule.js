import { fetchRegionStat } from "@/API/regions";

export const regionStatModule = {
    state() {
        return {
            currentRegionId: 1,
            regionStat: [],
            loaded: false
        }
    },
    mutations: {
        setRegionStat(state, stat) {
            state.regionStat = stat
        },
        setCurrentRegionId(state, regionId) {
            state.currentRegionId = regionId
        }
    },
    getters: {
    },
    actions: {
        getRegionStat: async function ({ commit, state }) {
            const { data } = await fetchRegionStat(state.currentRegionId)
            commit("setRegionStat", data.data)
        }
    },
    modules: {

    },
    namespaced: true
}