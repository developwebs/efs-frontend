import { createStore } from "vuex"
import { accountModule, adminModule, authModule, formsModule, locationModule, mainModule, mapModule, newsModule, organizationsModule, profileModule, regionStatModule, technicalSupportModule, usersModule } from "./modules"

export const store = createStore({
    state() {
        return {
            initialized: false
        }
    },
    mutations: {
        setInitialized(state, initizalized) {
            state.initialized = initizalized
        }
    },
    actions: {
        setInitialized({ commit }, initialized) {
            commit("setInitialized", initialized)
        }
    },
    modules: {
        main: mainModule,
        news: newsModule,
        auth: authModule,
        technicalSupport: technicalSupportModule,
        location: locationModule,
        regionStat: regionStatModule,
        organizations: organizationsModule,
        account: accountModule,
        profile: profileModule,
        forms: formsModule,
        map: mapModule,
        admin: adminModule,
        users: usersModule
    }
})